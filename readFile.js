const fs = require('fs');
const metrics = ['g', 'kg', 'ml', 'cl', 'dl', 'l','c.chá'];
const wordsToRemove = ['de ', 's/', 'talo', 'dentes', 'pele', 'pitada'];

module.exports = {
    parseList:  function () {
    var array = fs.readFileSync('ingredientes').toString().split("\n");

    let ingredients = [];
    
    for(i in array) {
        let line = array[i];
        if (!line.trim() || (line.indexOf('[') == 0 && line.lastIndexOf(']') == line.length-1 )) {
            // is empty or whitespace
            //or [.....]
        } else {
            let obj = handleLine(line);
            //console.log(obj);
            ingredients.push(obj);
        }
    }
    return ingredients;
}
}


function handleLine(line){
    wordsToRemove.forEach((word) => {
        line = line.replace(word,'').replace('  ',' ');
      }
    );

    line = line.replace('c. ', 'c.');

    let arr = line.split(' ');
    let qt;
    let ingredient;
    //console.log(arr);
    if(metrics.includes(arr[1])) {
        qt = arr.slice(0, 2).join(' ');
        ingredient = arr.slice(2).join(' ');
    } else {
        qt = arr[0];
        ingredient = arr.slice(1).join(' ');
    }
    //console.log(qt + ' - ' + ingredient);
    let obj = {};
    obj.name = ingredient;
    handleQuantity(obj, qt);
    //console.log(obj);
    return obj;
}

function handleQuantity(obj, qt){
    let arr = qt.split(' ');
    if(qt.includes('g')){
        
        //lets assume if less than 50g, we reaaly want some seasoning
        let weight = parseInt(arr[0]); 
        if(weight <= 50) {
            obj.isUnit = true;
            obj.units = 1;
            obj.isWeight = false;
        } else {
            obj.isWeight = true;
            obj.grams = weight;
            let kg = parseFloat(arr[0])/1000;
            obj.kilograms = (kg+'').replace('.',',');

            obj.isUnit = false;
            obj.units = 0;
        }

    } else if(qt.includes('kg')){

        obj.isWeight = true;
        obj.kilograms = arr[0];
        let kg = parseFloat(arr[0])*1000;
        obj.grams = (kg+'').replace('.',',');

        obj.isUnit = false;
        obj.units = 0;

    } else {
        //if array we usually deal with low amounts (spoons, pieces)
        if(Array.isArray(arr)){
            obj.isUnit = true;
            obj.units = 1;
            obj.isWeight = false;
        } else {
            obj.isUnit = true;
            obj.units = parseInt(arr);
            obj.isWeight = false;
        }
    }
}