const {Builder, By, Key, until} = require('selenium-webdriver');
const readFile = require('./readFile'); 
const redisHandler = require('./RedisHandler'); 
require('dotenv').config()

const username = process.env.CONTINENTE_USERNAME;
const password = process.env.CONTINENTE_PASSWORD;
const startpage = process.env.CONTINENTE_START_PAGE;
const ingredientURL = process.env.CONTINENTE_INGREDIENT_URL;
const conversionRegEx = /(?<=emb.)(.*)(?=gr)/gm;

var driver;


main();


async function main(){
  
 let ingredients = readFile.parseList();

 driver = await new Builder().forBrowser('chrome').build();
 await login();
  await driver.get(startpage);
  for(let i = 0; i < ingredients.length ; ++i) {
    let ingredient = ingredients[i];
    console.log('pick: ' + ingredient.name);
    await selectIngredient(ingredient);
  }
}

async function login(){

  try {
    await driver.get(startpage);
    try {
      await driver.wait(until.elementLocated(By.id("fancybox-close")), 10000);
      await driver.findElement(By.id('fancybox-close')).click();
    }catch(error){}

    await driver.findElement(By.id('username')).sendKeys(username);
    await driver.findElement(By.id('password')).sendKeys(password);
    await driver.findElement(By.id('btnLogin')).click();

    try {
      await driver.wait(until.elementLocated(By.id("blockUIClose")), 10000);
      await driver.findElement(By.id('blockUIClose')).click();
    }catch(error){}

  } finally {
    //await driver.quit();
  }
}

async function selectIngredient(ingredient){

  try {
    let prodId = await redisHandler.getKey(ingredient.name);
    console.log('get key'+ingredient.name+' -> ' + prodId);
    if(prodId){
      await driver.get(ingredientURL+prodId);
    } else {
      await driver.findElement(By.id('globalSearch')).sendKeys(Key.chord(Key.COMMAND, 'a'));
      await driver.findElement(By.id('globalSearch')).sendKeys(Key.BACK_SPACE);
      await driver.findElement(By.id('globalSearch')).sendKeys(ingredient.name, Key.RETURN);

      await driver.wait(until.urlContains(ingredientURL));

      let curr = await driver.getCurrentUrl();
      let productId = curr.replace(ingredientURL,'');
      console.log('prodId from browser: ' + productId);
      redisHandler.setKey(ingredient.name, productId);
    }
    
    if(ingredient.isWeight) {
      let isWeightBoxAvailable = await driver.findElement(By.className('firstQuantityTextBox')).then(function(webElement) {return true;}, function(err) {return false;});
      
      if(isWeightBoxAvailable) {
        let weightBox = await driver.findElement(By.className('firstQuantityTextBox'));
        await weightBox.sendKeys(Key.chord(Key.COMMAND, 'a'));
        await weightBox.sendKeys(Key.BACK_SPACE);
        await weightBox.sendKeys(ingredient.kilograms);
      } else {
        let conversion = await driver.findElement(By.className('productSubsubtitle')).getText();
        let ratioExp = conversion.match(conversionRegEx);
        let ratio = parseInt((''+ratioExp).trim());
        let units = ingredient.grams / ratio;
        let unitsTextBox = await driver.findElement(By.className('unitsTextBox'))
        await unitsTextBox.sendKeys(Key.chord(Key.COMMAND, 'a'));
        await unitsTextBox.sendKeys(Key.BACK_SPACE);
        await unitsTextBox.sendKeys(Math.round(units));
      }
    } else {
      let unitsTextBox = await driver.findElement(By.className('unitsTextBox'))
      await unitsTextBox.sendKeys(Key.chord(Key.COMMAND, 'a'));
      await unitsTextBox.sendKeys(Key.BACK_SPACE);
      await unitsTextBox.sendKeys(ingredient.units);
      
    }

    try {
    await driver.findElement(By.className('add-ToBasket')).click();
  }catch(error){console.error('CLICK: ' + error);}
    
    //deal with minimum quatities
    try {
      await driver.wait(until.elementLocated(By.id("blockUICloseBasketChanges")), 500);
      await driver.findElement(By.id('blockUICloseBasketChanges')).click();
    }catch(error){console.error('XXXXXX' + error);return;}
    

  }catch(error){
    console.error(error);
  }
  finally {
    //await driver.quit();
  }
}
