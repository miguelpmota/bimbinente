# Bimbinente #

A js script that uses Cookidoo (https://cookidoo.pt/) (or any other following the same format) shopping list to place an order in Continente Online (https://www.continente.pt/).

### Requirements ###

* Chromedriver
* node modules: fs, redis, util, selenium-webdriver, dotenv
install the modules using "npm install --save MODULE_NAME"
* initialize the project with npm init

### Set Up ###

* Environment Variables:
  - CONTINENTE_USERNAME: your continente.pt username
  - CONTINENTE_PASSWORD: your continente.pt password
  - CONTINENTE_START_PAGE: https://www.continente.pt/pt-pt/public/Pages/homepage.aspx
  - CONTINENTE_INGREDIENT_URL: https://www.continente.pt/stores/continente/pt-pt/public/Pages/ProductDetail.aspx?ProductId=
  - REDIS_URL: your redis connection string if you want to save your previous choices
