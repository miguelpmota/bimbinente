const redis_url = process.env.REDIS_URL;
const redis = require('redis');
const client = redis.createClient(redis_url);
//const client = require('redis').createClient(process.env.REDIS_URL);
const {promisify} = require('util');
const getAsync = promisify(client.get).bind(client);

let ignoreRedis = false;

client.on('error', function(err){
    console.log('Something went wrong ', err);
    ignoreRedis = true;
    client.end(true);
});



module.exports = {
    getKey:  async function (key) {
        if(ignoreRedis) return null;
        return await getAsync(key);
    },
    setKey:  function (key, value) {
        if(ignoreRedis) return null
        console.log(key + ' - ' + value);
        client.set(key, value, redis.print);
    },
    clearAll: function() {
        if(ignoreRedis) return null
        client.flushdb( function (err, succeeded) {
            console.log(succeeded); // will be true if successfull
        });
    }
    
}